#!/usr/bin/python3

# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import argparse
import os
import sys
import subprocess
from pathlib import Path
from traceutil import iter_trace_paths, trace_has_images, all_trace_type_names
from traceutil import trace_type_from_name, trace_type_from_filename, TraceType

def log(severity, msg, end='\n'):
    print("[dump_trace_images] %s: %s" % (severity, msg), flush=True, end=end)

def log_result(msg):
    print(msg, flush=True)

def run_logged_command(cmd, log_path):
    ret = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logoutput = ("[dump_trace_images] Running: %s\n" % " ".join(cmd)).encode() + \
                ret.stdout
    log_path.parent.mkdir(parents=True, exist_ok=True)
    with log_path.open(mode='wb') as log:
        log.write(logoutput)
    if ret.returncode:
        raise RuntimeError(
            logoutput.decode(errors='replace') +
            "[dump_traces_images] Process failed with error code: %d" % ret.returncode)

def get_calls_num(trace, device_name):
    tracename = str(Path(trace).name)
    refdir = str(Path(trace).parent / "references" / device_name)
    calls = []
    for root, dirs, files in os.walk(refdir):
        for file in files:
            if file.startswith(tracename) and file.endswith(".png"):
                callnum = file[(len(tracename) + 1):-4]
                calls.append(callnum)
    return calls

def dump_with_apitrace(trace, calls, device_name):
    outputdir = str(Path(trace).parent / "test" / device_name)
    os.makedirs(outputdir, exist_ok=True)
    outputprefix = str(Path(outputdir) / Path(trace).name) + "-"
    cmd = ["apitrace", "dump-images", "--calls=" + ','.join(calls),
           "-o", outputprefix, trace]
    log_path = Path(outputdir) / (Path(trace).name + ".log")
    run_logged_command(cmd, log_path)

def dump_with_renderdoc(trace, calls, device_name):
    outputdir = str(Path(trace).parent / "test" / device_name)
    script_path = Path(os.path.dirname(os.path.abspath(__file__)))
    cmd = [str(script_path / "renderdoc_dump_images.py"), trace, outputdir]
    cmd.extend(calls)
    log_path = Path(outputdir) / (Path(trace).name + ".log")
    run_logged_command(cmd, log_path)

def dump_from_trace(trace, device_name):
    log("Info", "Dumping trace %s" % trace, end='... ')
    calls = get_calls_num(trace, device_name)
    trace_type = trace_type_from_filename(trace)
    try:
        if trace_type == TraceType.APITRACE:
            dump_with_apitrace(trace, calls, device_name)
        elif trace_type == TraceType.RENDERDOC:
            dump_with_renderdoc(trace, calls, device_name)
        else:
            raise RuntimeError("Unknown tracefile extension")
        log_result("OK")
        return True
    except Exception as e:
        log_result("ERROR")
        log("Debug", "=== Failure log start ===")
        print(e)
        log("Debug", "=== Failure log end ===")
        return False

def filter_trace_paths_with_ref_images(trace_paths, device_name):
    out = []
    for trace_path in trace_paths:
        if trace_has_images(trace_path, str(Path("references") / device_name)):
            out.append(str(trace_path))
        else:
            log("Warning", "%s has no reference images, skipping" % str(trace_path))
    return out

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('tracepath', help="single trace or dir to walk recursively")
    parser.add_argument('--device-name', required=True,
                        help="the name of the graphics device used to produce images")
    parser.add_argument('--trace-types', required=False,
                        default=",".join(all_trace_type_names()),
                        help="the types of traces to look for in recursive dir walks "
                             "(by default all types)")

    args = parser.parse_args()
    trace_types = [trace_type_from_name(t) for t in args.trace_types.split(",")]

    if os.path.isdir(args.tracepath):
        all_trace_paths = iter_trace_paths(args.tracepath, trace_types)
    elif os.path.isfile(args.tracepath):
        all_trace_paths = [Path(args.tracepath)]

    traces = filter_trace_paths_with_ref_images(all_trace_paths,
                                                args.device_name)

    failed_dump = False

    for trace in traces:
        if not dump_from_trace(trace, args.device_name):
            failed_dump = True

    sys.exit(1 if failed_dump else 0)

if __name__ == "__main__":
    main()
