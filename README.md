Tracie - Mesa Traces Continuous Integration System
==================================================

Home of the Mesa trace testing effort prototype.

## CI internals

### Docker image setup

The Mesa trace testing prototype is run inside a specially prepared docker
image. The image contains the latest dependencies needed to build Mesa and also
the tools for trace replay (currently apitrace and renderdoc).

Image setup uses helpers from the debian.yml template from
https://gitlab.freedesktop.org/wayland/ci-templates. This template is included
by our own yaml file.

The code to setup the image is based on the respective code from the Mesa
repository and resides in:
[.gitlab-ci/debian-install.sh](.gitlab-ci/debian-install.sh)

The images are cached based on the image tag, so any changes to to the install
script should be accompanied by a change to the `DEBIAN_TAG` variable in
[.gitlab-ci.yml](.gitlab-ci.yml)

### Building Mesa

By default we build and test Mesa master, but this can be overriden in manual
CI runs by setting the `MESA_COMMIT` and `MESA_REPO` variables.

### Traces

The traces are held in a separate git repository, which is cloned during CI
testing:

    https://gitlab.freedesktop.org/gfx-ci/tracie/traces-db

Each trace is accompanied by a set of reference images in a subdirectory named
`references`. The reference images are of the form `tracefilename-callnum.png`.

The repository and commit to use for traces-db can be overriden in manual
CI runs by using the `TRACES_DB_REPO` and `TRACES_DB_COMMIT` CI variables.

### Replaying traces

Mesa traces CI uses a set of scripts to replay traces and check the output
against the reference images. We currently replay the traces with software
rendering under the Xvfb X server.

The [scripts/dump_trace_images.py](scripts/dump_trace_images.py) script replays
traces, dumping the images from calls in the replay that match the expected
reference images.  The dumped images are stored in a subdirectory `test` next
to the `references` directory and use the same filename format. The full log of
any commands used while dumping the images is also saved in a file in the
'test' subdirectory, named after the trace name with '.log' appended. The
script accepts either a single trace (.trace or .rdc), or a directory. In the
latter case it recursively walks the directory structure and replays all traces
it finds.

Examples:

    python3 dump_traces_images.py tracesdir
    python3 dump_traces_images.py mytrace.trace

The [scripts/diff_trace_images.py](scripts/diff_trace_images.py) script
compares the dumped test images of a trace against the reference images. It
then writes out a report, including the images and also any diff between them.
The script accepts either a single trace (.trace or .rdc), or a directory. In
the latter case it recursively walks the directory structure and compares
images for all traces it finds.

Examples:

    python3 diff_traces_images.py tracesdir reportdir
    python3 diff_traces_images.py mytrace.trace reportdir

### Running the replay scripts locally

It's often useful, especially during development, to be able to run the scripts
locally.

The [scripts/dump_trace_images.py](scripts/dump_trace_images.py) script depends
on a recent version of apitrace being in the path, and also the renderdoc
python module being available.

The [scripts/diff_trace_images.py](scripts/diff_trace_images.py) script depends
on a recent version of apitrace being in the path.

For some guidance for building apitrace and renderdoc, check the
[gitlab-ci/debian-install.sh](.gitlab-ci/debian-install.sh) script.

To ensure python3 can find the renderdoc python module you need to set
`PYTHONPATH` to point to the location of `renderdoc.so` (binary python modules)
and `LD_LIBRARY_PATH` to point to the location of `librenderdoc.so`. In the
renderdoc build tree, both of these are in `renderdoc/<builddir>/lib`. Note
that renderdoc doesn't install the `renderdoc.so` python module.
