#!/usr/bin/env bash

create_clean_git()
{
    rm -rf .clean_git
    cp -R .git .clean_git
}

restore_clean_git()
{
    rm -rf .git
    cp -R .clean_git .git
}

fetch_trace()
{
    _trace="${1//,/?}"
    echo -n "[fetch_trace] Fetching $1... "
    _output=$(git lfs pull -I "$_trace" 2>&1)
    _ret=0
    if [[ $? -ne 0 || ! -f "$1" ]]; then
        echo "ERROR"
        echo "$_output"
        _ret=1
    else
        echo "OK"
    fi
    restore_clean_git
    return $_ret
}

fetch_reference_images()
{
    _device_name="$1"
    _trace_type="$2"
    _wildcard="**/$_device_name/*$_trace_type*.png"
    echo -n "[fetch_trace] Fetching reference images '$_wildcard'... "
    _output=$(git lfs pull -I "$_wildcard" 2>&1)
    _ret=0
    if [[ $? -ne 0 ]]; then
        echo "ERROR"
        echo "$_output"
        _ret=1
    else
        echo "OK"
    fi
    restore_clean_git
    return $_ret
}

# During git operations various git objects get created which
# may take up significant space. Store a clean .git instance,
# which we restore after various git operations to keep our
# storage consumption low.
create_clean_git

fetch_reference_images "$DEVICE_NAME" "$1" || exit $?

ret=0

for trace in $(git lfs ls-files -n -I \*.$1)
do
    python3 "$CI_PROJECT_DIR/scripts/trace_has_ref_images.py" --device-name "$DEVICE_NAME" "$trace" || { echo "[fetch_trace] Skipping $trace since it has no reference images"; continue; }
    fetch_trace "$trace" || exit $?
    python3 "$CI_PROJECT_DIR/scripts/dump_trace_images.py" --device-name "$DEVICE_NAME" "$trace" || exit $?
    python3 "$CI_PROJECT_DIR/scripts/diff_trace_images.py" --device-name "$DEVICE_NAME" --output-dir "$CI_PROJECT_DIR/results" "$trace" || ret=1
    rm "$trace"
done

exit $ret
